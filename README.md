## Enunciado del Ejercicio: Implementación de GitFlow Artesanal con Despliegue en GitLab Pages

### Objetivo
El objetivo de este ejercicio es aprender a implementar la metodología GitFlow de manera artesanal y configurar un pipeline en GitLab para el despliegue de un sitio web estático utilizando GitLab Pages.

### Descripción
En este ejercicio, se te proporcionará la estructura base de un sitio web simple utilizando HTML y Bootstrap. Deberás seguir los pasos detallados para configurar un flujo de trabajo basado en GitFlow manualmente, sin usar herramientas automatizadas, y crear un pipeline de GitLab CI/CD para desplegar el sitio web en GitLab Pages.

### Requisitos

1. **Cuenta en GitLab**: Necesitarás una cuenta en GitLab para realizar este ejercicio.
2. **Conocimientos básicos de Git**: Se espera que tengas conocimientos previos sobre cómo usar Git para el control de versiones.
3. **Herramientas de desarrollo**: Asegúrate de tener instalados Git y un editor de texto o IDE de tu preferencia.

### Pasos a Seguir

1. **Crear un nuevo proyecto en GitLab**: Inicia sesión en tu cuenta de GitLab y crea un nuevo proyecto.
2. **Clonar el repositorio en tu máquina local**: Clona el repositorio recién creado en tu máquina local.
3. **Configurar ramas base**: Crea y configura las ramas principales `main` y `develop`.
4. **Crear una rama para features**: Crea una rama para desarrollar una nueva funcionalidad en el sitio web.
5. **Implementar cambios en la rama de feature**: Realiza los cambios necesarios en la rama de feature y súbelos al repositorio remoto.
6. **Crear un Merge Request (MR) en GitLab**: Solicita la integración de los cambios realizados en la rama de feature a la rama `develop` mediante un Merge Request.
7. **Preparar una release**: Crea una rama de release para preparar el despliegue de una nueva versión del sitio web.
8. **Fusionar la release en `main` y `develop`**: Integra los cambios de la rama de release tanto en `main` como en `develop`.
9. **Crear un tag para la release**: Etiqueta la nueva versión del sitio web.
10. **Configurar el archivo `.gitlab-ci.yml`**: Configura el pipeline de GitLab CI/CD para el despliegue en GitLab Pages.
11. **Verificar y ejecutar el pipeline**: Verifica que el pipeline se ejecute correctamente y despliegue el sitio web en GitLab Pages.
12. **Acceder a GitLab Pages**: Accede a la URL proporcionada por GitLab Pages para visualizar tu sitio web desplegado.

### Materiales Proporcionados

- **Estructura base del sitio web (index.html)**:
  ```html
  <!doctype html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Bootstrap demo</title>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    </head>
    <body>
      <h1>Hello, world!</h1>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    </body>
  </html>
  ```

- **Configuración del pipeline (archivo `.gitlab-ci.yml`)**:
  ```yaml
  pages:
    stage: deploy
    script:
      - apt-get update -y && apt-get install -y rsync
      - rm -rf public
      - mkdir public
      - rsync -av --exclude='public' ./ public/
    artifacts:
      paths:
        - public
    only:
      - main
  ```

### Entregables

- **Repositorio en GitLab**: Un repositorio en GitLab con todas las ramas y configuraciones realizadas.
- **Pipeline funcional**: Un pipeline de GitLab CI/CD funcional que despliegue correctamente el sitio web en GitLab Pages.
- **URL del sitio web desplegado**: La URL del sitio web desplegado en GitLab Pages.